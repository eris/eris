/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/sheetgrid.hpp"
#include "eris/rect.hpp"
#include "eris/frame.hpp"
#include "eris/drawing_context.hpp"

#define _USE_MATH_DEFINES 1
#include <math.h>

#include <windows.h>

namespace eris {

SheetGrid::SheetGrid(Window* parent, int identifier) :
    Window(parent, identifier)
{
}

SheetGrid::~SheetGrid()
{
}

void SheetGrid::paint(DrawingContext& dc)
{
    cairo_t* cr = dc.mpCr;
    Rect size = getRect();
    cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);

    // Draw background first.
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_rectangle(cr, size.left, size.top, size.right, size.bottom);
    cairo_fill(cr);

    cairo_set_source_rgb (cr, 0.78, 0.78, 0.78);
    cairo_set_line_width (cr, 1);

    // Draw horizontal grid lines.
    long rowHeight = 20;
    for (long y = size.top + rowHeight; y < size.bottom; y += rowHeight)
    {
        cairo_move_to(cr, size.left, y);
        cairo_line_to(cr, size.right, y);
    }

    // Draw vertical grid lines.
    long colWidth = 70;
    for (long x = size.left; x < size.right; x += colWidth)
    {
        cairo_move_to(cr, x, size.top);
        cairo_line_to(cr, x, size.bottom);
    }

    cairo_stroke(cr);

    cairo_set_antialias(cr, CAIRO_ANTIALIAS_DEFAULT);
    double x = size.left;
    double y = size.top;
    {
        double xc = 528.0;
        double yc = 128.0;
        double radius = 100.0;
        double angle1 = 45.0  * (M_PI/180.0);  /* angles are specified */
        double angle2 = 180.0 * (M_PI/180.0);  /* in radians           */

        cairo_set_line_width (cr, 10.0);
        cairo_arc (cr, x+xc, y+yc, radius, angle1, angle2);
        cairo_stroke (cr);

        /* draw helping lines */
        cairo_set_source_rgba (cr, 1, 0.2, 0.2, 0.6);
        cairo_set_line_width (cr, 6.0);

        cairo_arc (cr, x+xc, y+yc, 10.0, 0, 2*M_PI);
        cairo_fill (cr);

        cairo_arc (cr, x+xc, y+yc, radius, angle1, angle1);
        cairo_line_to (cr, x+xc, y+yc);
        cairo_arc (cr, x+xc, y+yc, radius, angle2, angle2);
        cairo_line_to (cr, x+xc, y+yc);
        cairo_stroke (cr);
    }

    {
        double x0 = x + 225.6,   /* parameters like cairo_rectangle */
        y0 = y + 225.6,
        rect_width  = 204.8,
        rect_height = 204.8,
        radius = 102.4;   /* and an approximate curvature radius */

        double x1,y1;

        x1=x0+rect_width;
        y1=y0+rect_height;
        if (!rect_width || !rect_height)
            return;
        if (rect_width/2<radius)
        {
            if (rect_height/2<radius)
            {
                cairo_move_to  (cr, x0, (y0 + y1)/2);
                cairo_curve_to (cr, x0 ,y0, x0, y0, (x0 + x1)/2, y0);
                cairo_curve_to (cr, x1, y0, x1, y0, x1, (y0 + y1)/2);
                cairo_curve_to (cr, x1, y1, x1, y1, (x1 + x0)/2, y1);
                cairo_curve_to (cr, x0, y1, x0, y1, x0, (y0 + y1)/2);
            }
            else
            {
                cairo_move_to  (cr, x0, y0 + radius);
                cairo_curve_to (cr, x0 ,y0, x0, y0, (x0 + x1)/2, y0);
                cairo_curve_to (cr, x1, y0, x1, y0, x1, y0 + radius);
                cairo_line_to (cr, x1 , y1 - radius);
                cairo_curve_to (cr, x1, y1, x1, y1, (x1 + x0)/2, y1);
                cairo_curve_to (cr, x0, y1, x0, y1, x0, y1- radius);
            }
        }
        else
        {
            if (rect_height/2<radius)
            {
                cairo_move_to  (cr, x0, (y0 + y1)/2);
                cairo_curve_to (cr, x0 , y0, x0 , y0, x0 + radius, y0);
                cairo_line_to (cr, x1 - radius, y0);
                cairo_curve_to (cr, x1, y0, x1, y0, x1, (y0 + y1)/2);
                cairo_curve_to (cr, x1, y1, x1, y1, x1 - radius, y1);
                cairo_line_to (cr, x0 + radius, y1);
                cairo_curve_to (cr, x0, y1, x0, y1, x0, (y0 + y1)/2);
            }
            else
            {
                cairo_move_to  (cr, x0, y0 + radius);
                cairo_curve_to (cr, x0 , y0, x0 , y0, x0 + radius, y0);
                cairo_line_to (cr, x1 - radius, y0);
                cairo_curve_to (cr, x1, y0, x1, y0, x1, y0 + radius);
                cairo_line_to (cr, x1 , y1 - radius);
                cairo_curve_to (cr, x1, y1, x1, y1, x1 - radius, y1);
                cairo_line_to (cr, x0 + radius, y1);
                cairo_curve_to (cr, x0, y1, x0, y1, x0, y1- radius);
            }
        }
        cairo_close_path (cr);

        cairo_set_source_rgb (cr, 0.5, 0.5, 1);
        cairo_fill_preserve (cr);
        cairo_set_source_rgba (cr, 0.5, 0, 0, 0.5);
        cairo_set_line_width (cr, 10.0);
        cairo_stroke (cr);
    }

    {
        cairo_pattern_t *pat;

        pat = cairo_pattern_create_linear (0.0, 0.0,  0.0, 256.0);
        cairo_pattern_add_color_stop_rgba (pat, 1, 0, 0, 0, 1);
        cairo_pattern_add_color_stop_rgba (pat, 0, 1, 1, 1, 1);
        cairo_rectangle (cr, x, x, x+256, y+256);
        cairo_set_source (cr, pat);
        cairo_fill (cr);
        cairo_pattern_destroy (pat);

        pat = cairo_pattern_create_radial (115.2, 102.4, 25.6,
                                           102.4,  102.4, 128.0);
        cairo_pattern_add_color_stop_rgba (pat, 0, 1, 1, 1, 1);
        cairo_pattern_add_color_stop_rgba (pat, 1, 0, 0, 0, 1);
        cairo_set_source (cr, pat);
        cairo_arc (cr, x+128.0, y+128.0, 76.8, 0, 2 * M_PI);
        cairo_fill (cr);
        cairo_pattern_destroy (pat);
    }
}

void SheetGrid::init()
{
}

}
