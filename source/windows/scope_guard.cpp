/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/windows/scope_guard.hpp"

namespace eris {

PaintGuard::PaintGuard(HWND hwnd) : mhWnd(hwnd)
{
    mhDC = BeginPaint(mhWnd, &mPS);
}

PaintGuard::~PaintGuard()
{
    EndPaint(mhWnd, &mPS);
}

HDC PaintGuard::get()
{
    return mhDC;
}

DCGuard::DCGuard(HWND hwnd) : mhWnd(hwnd)
{
    mhDC = GetDC(mhWnd);
}

DCGuard::~DCGuard()
{
    if (mhDC)
        ReleaseDC(mhWnd, mhDC);
}

HDC DCGuard::get() { return mhDC; }

CompatibleDCGuard::CompatibleDCGuard(HDC hdc) : mhOriginalDC(hdc)
{
    mhNewDC = CreateCompatibleDC(mhOriginalDC);
}

CompatibleDCGuard::~CompatibleDCGuard()
{
    if (mhNewDC)
        DeleteDC(mhNewDC);
}

HDC CompatibleDCGuard::get() { return mhNewDC; }

ThemeDataGuard::ThemeDataGuard(HWND hwnd, LPCWSTR clsList)
{
    mhTheme = OpenThemeData(hwnd, clsList);
}

ThemeDataGuard::~ThemeDataGuard()
{
    if (mhTheme)
        CloseThemeData(mhTheme);
}

HTHEME ThemeDataGuard::get() { return mhTheme; }

DIBSectionGuard::DIBSectionGuard(
    HDC hdc, HDC hdcCompat, const BITMAPINFO *pbmi, UINT iUsage, VOID **ppvBits,
    HANDLE hSection, DWORD dwOffset) :
    mhDIBOld(NULL), mhDCCompat(hdcCompat)
{
    mhDIB = CreateDIBSection(hdc, pbmi, iUsage, ppvBits, hSection, dwOffset);
    if (mhDIB)
        mhDIBOld = static_cast<HBITMAP>(SelectObject(hdcCompat, mhDIB));
}

DIBSectionGuard::~DIBSectionGuard()
{
    if (mhDIB)
        DeleteObject(mhDIB);
    if (mhDIBOld)
        SelectObject(mhDCCompat, mhDIBOld);
}

HBITMAP DIBSectionGuard::get() { return mhDIB; }

FontGuard::FontGuard(HDC hdc, HFONT font) : mhDC(hdc), mhFontOld(NULL)
{
    if (font)
        SelectObject(mhDC, font);
}

FontGuard::~FontGuard()
{
    if (mhFontOld)
        SelectObject(mhDC, mhFontOld);
}

void FontGuard::set(HFONT font)
{
    mhFontOld = (HFONT)SelectObject(mhDC, font);
}

CompatibleBitmapGuard::CompatibleBitmapGuard(HDC hdc, HDC hdcbuf, int width, int height) : mhBuf(NULL)
{
    mhBuf = CreateCompatibleBitmap(hdc, width, height);
    SelectObject(hdcbuf, mhBuf);
}

CompatibleBitmapGuard::~CompatibleBitmapGuard()
{
    if (mhBuf)
        DeleteObject(mhBuf);
}

CairoContextGuard::CairoContextGuard(HDC hdc) : mpSurface(NULL), mpCr(NULL)
{
    if (hdc)
    {
        mpSurface = cairo_win32_surface_create(hdc);
        mpCr = cairo_create(mpSurface);
    }
}

CairoContextGuard::~CairoContextGuard()
{
    if (mpCr)
        cairo_destroy(mpCr);
    if (mpSurface)
        cairo_surface_destroy(mpSurface);
}

cairo_t* CairoContextGuard::get() { return mpCr; }

}
