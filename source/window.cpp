/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/window.hpp"
#include "eris/rect.hpp"

#include <vector>

namespace eris {

typedef std::vector<Window*> ChildWindowsType;

struct WindowImpl
{
    Rect maSize;
    ChildWindowsType maChildWnds;
    ChildWindowsType::iterator miCurChild;
    Window* mpParent;
    int mnID;

    WindowImpl(Window* parent, int identifier);
};

WindowImpl::WindowImpl(Window* parent, int identifier) :
    mpParent(parent),
    mnID(identifier) {}

Window::Window(Window* parent, int identifier) :
    mpImpl(new WindowImpl(parent, identifier))
{
    if (parent)
        parent->addChild(this);
}

Window::~Window()
{
    delete mpImpl;
}

void Window::onMouseMove(const MouseEvent&)
{
}

void Window::onLButtonUp(const MouseEvent&)
{
}

void Window::onLButtonDown(const MouseEvent&)
{
}

void Window::paint(DrawingContext& dc) {}

void Window::setRect(const Rect& rect)
{
    mpImpl->maSize = rect;
}

Rect Window::getRect() const
{
    return mpImpl->maSize;
}

void Window::addChild(Window* wnd)
{
    mpImpl->maChildWnds.push_back(wnd);
}

Frame* Window::getFrame()
{
    for (Window* p = mpImpl->mpParent; p; p = p->getParent())
    {
        Frame* frame = p->getFrame();
        if (frame)
            return frame;
    }
    return NULL;
}

Window* Window::getParent()
{
    return mpImpl->mpParent;
}

Window* Window::firstChild()
{
    mpImpl->miCurChild = mpImpl->maChildWnds.begin();
    return mpImpl->miCurChild == mpImpl->maChildWnds.end() ? NULL : *mpImpl->miCurChild;
}

Window* Window::nextChild()
{
    ++mpImpl->miCurChild;
    return mpImpl->miCurChild == mpImpl->maChildWnds.end() ? NULL : *mpImpl->miCurChild;
}

int Window::getIdentifier() const
{
    return mpImpl->mnID;
}

}
