/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/menubar.hpp"
#include "eris/rect.hpp"
#include "eris/frame.hpp"
#include "eris/mouseevent.hpp"
#include "eris/menupopup.hpp"
#include "eris/windows/scope_guard.hpp"
#include "eris/drawing_context.hpp"

#include <vssym32.h>

#include <limits>

#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/scoped_ptr.hpp>

namespace eris {

namespace {

size_t NOT_SELECTED = 1000;

}

struct MenuBarImpl
{
    typedef boost::ptr_vector<MenuBarItem> ItemsType;
    Frame* mpFrame;
    ItemsType maItems;
    size_t mnCurItem;

    boost::scoped_ptr<MenuPopup> mpMenuPopup;

    MenuBarImpl() : mnCurItem(NOT_SELECTED) {}
};

MenuBar::MenuBar(Frame* frame, int identifier) :
    Window(NULL, identifier),
    mpImpl(new MenuBarImpl)
{
    mpImpl->mpFrame = frame;
}

MenuBar::~MenuBar()
{
    delete mpImpl;
}

Frame* MenuBar::getFrame()
{
    return mpImpl->mpFrame;
}

namespace {

void paintMenuBarItem(HDC hdc, HDC hdcbuf, MenuBarItem& item)
{
    const Rect& rect = item.getRect();

    BITMAPINFO dib = { 0 };
    dib.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
    dib.bmiHeader.biWidth       = rect.width();
    dib.bmiHeader.biHeight      = -rect.height();
    dib.bmiHeader.biPlanes      = 1;
    dib.bmiHeader.biBitCount    = 32;
    dib.bmiHeader.biCompression = BI_RGB;
    DIBSectionGuard dibGuard(hdc, hdcbuf, &dib, DIB_RGB_COLORS, NULL, NULL, 0);
    CairoContextGuard crGuard(hdcbuf);
    cairo_t* cr = crGuard.get();

    DrawingContext dc;
    dc.mpCr = cr;
    dc.mhDC = hdcbuf;
    dc.maOffset.x = -rect.left;
    dc.maOffset.y = -rect.top;

    item.paint(dc);

    // Now blit the object to the screen
    BitBlt(hdc, rect.left, rect.top, rect.width(), rect.height(), hdcbuf, 0, 0, SRCCOPY);
}

}

void MenuBar::onMouseMove(const MouseEvent& evt)
{
    // Keep track of currently selected menu bar item.
    MenuBarImpl::ItemsType& items = mpImpl->maItems;
    size_t itemCount = mpImpl->maItems.size();
    const Point& pt = evt.getPos();
    size_t selected = NOT_SELECTED;
    for (size_t i = 0; i < itemCount; ++i)
    {
        const MenuBarItem& item = items[i];
        const Rect& rect = item.getRect();
        if (rect.contains(pt))
        {
            selected = i;
            break;
        }
    }

    if (mpImpl->mnCurItem == selected)
        return;

    Frame* frame = getFrame();
    if (!frame)
        return;

    DCGuard dcGuard(frame->mhWnd);
    HDC hdc = dcGuard.get();

    CompatibleDCGuard compDCGuard(hdc);
    HDC hdcbuf = compDCGuard.get();

    if (mpImpl->mnCurItem < itemCount)
    {
        MenuBarItem& item = items[mpImpl->mnCurItem];
        item.setState(MenuBarItem::Normal);
        paintMenuBarItem(hdc, hdcbuf, item);
    }
    mpImpl->mnCurItem = selected;
    if (mpImpl->mnCurItem < itemCount)
    {
        MenuBarItem& item = items[mpImpl->mnCurItem];
        item.setState(MenuBarItem::Hovered);
        paintMenuBarItem(hdc, hdcbuf, item);
    }

#if 0
    Frame* frame = getFrame();
    if (!frame)
        return;

    DCGuard dcGuard(frame->mhWnd);
    HDC hdc = dcGuard.get();

    CompatibleDCGuard compDCGuard(hdc);
    HDC hdcbuf = compDCGuard.get();

    CompatibleBitmapGuard bmGuard(hdc, hdcbuf, 2, 2);
    CairoContextGuard crGuard(hdcbuf);
    cairo_t* cr = crGuard.get();

    cairo_set_source_rgb(cr, 0.1, 1, 1);
    cairo_rectangle(cr, 0, 0, 2, 2);
    cairo_stroke(cr);

    // Now blit the object to the screen
    BitBlt(hdc, evt.getPos().x, evt.getPos().y, 2, 2, hdcbuf, 0, 0, SRCCOPY);
#endif
}

void MenuBar::onLButtonDown(const MouseEvent& evt)
{
    if (mpImpl->mnCurItem == NOT_SELECTED)
        return;

    // To get the screen position of the main window.
    RECT rcWin;
    GetWindowRect(mpImpl->mpFrame->mhWnd, &rcWin);

    const MenuBarItem& item = mpImpl->maItems.at(mpImpl->mnCurItem);
    const Rect& rectItem = item.getRect();
    Rect rectPopup;
    rectPopup.left = rcWin.left + rectItem.left;
    rectPopup.top = rcWin.top + rectItem.bottom;
    rectPopup.right = rectPopup.left + 230;
    rectPopup.bottom = rectPopup.top + 150;
    mpImpl->mpMenuPopup.reset(new MenuPopup(mpImpl->mpFrame));
    MenuPopup& popup = *mpImpl->mpMenuPopup;
    popup.setRect(rectPopup);
    popup.init(item.getMenuItems());
    popup.beginPopup();
}

void MenuBar::onLButtonUp(const MouseEvent& evt)
{
}

namespace {

class PaintItem : std::unary_function<MenuBarItem, void>
{
    DrawingContext& mDC;
public:
    PaintItem(DrawingContext& dc) : mDC(dc) {}
    void operator() (MenuBarItem& item)
    {
        item.paint(mDC);
    }
};

}

void MenuBar::paint(DrawingContext& dc)
{
#if 0
    const Rect& rect = getRect();
    cairo_t* cr = dc.mpCr;
    cairo_set_source_rgb(cr, 0.2, 0.2, 0.2);
    cairo_set_line_width(cr, 0.5);
    cairo_rectangle(cr, rect.left, rect.top, rect.width(), rect.height());
    cairo_stroke(cr);
#endif
    std::for_each(mpImpl->maItems.begin(), mpImpl->maItems.end(), PaintItem(dc));
}

void MenuBar::init(const MenuConfig& config)
{
    const MenuConfig::TopCategoriesType& topCats = config.getTopCategories();
    MenuConfig::TopCategoriesType::const_iterator it = topCats.begin(), itEnd = topCats.end();
    for (; it != itEnd; ++it)
    {
        const MenuCategory& cat = *it;
        MenuBarItem* p = new MenuBarItem(this, 0);
        p->init(cat);
        mpImpl->maItems.push_back(p);
    }
}

void MenuBar::resizeItems()
{
    const Rect& barRect = getRect();
    Rect rect(barRect.left, barRect.top, barRect.left+60, barRect.bottom);
    MenuBarImpl::ItemsType::iterator i = mpImpl->maItems.begin(), ie = mpImpl->maItems.end();
    for (; i != ie; ++i)
    {
        i->setRect(rect);
        rect.move(60, 0);
    }
}

void MenuBar::reset()
{
    if (mpImpl->mnCurItem != NOT_SELECTED && mpImpl->mnCurItem < mpImpl->maItems.size())
    {
        Frame* frame = getFrame();
        if (!frame)
            return;

        DCGuard dcGuard(frame->mhWnd);
        HDC hdc = dcGuard.get();

        CompatibleDCGuard compDCGuard(hdc);
        HDC hdcbuf = compDCGuard.get();

        MenuBarItem& item = mpImpl->maItems[mpImpl->mnCurItem];
        item.setState(MenuBarItem::Normal);
        paintMenuBarItem(hdc, hdcbuf, item);

        mpImpl->mnCurItem = NOT_SELECTED;
    }
}

void MenuBar::terminate()
{
    if (mpImpl->mpMenuPopup)
    {
        mpImpl->mpMenuPopup->terminate();
        mpImpl->mpMenuPopup.reset();
    }
}

struct MenuBarItemImpl
{
    const MenuCategory* mpCategory;
    MenuBarItem::State meState;

    MenuBarItemImpl() : meState(MenuBarItem::Normal) {}
};

MenuBarItem::MenuBarItem(Window* parent, int identifier) :
    Window(parent, identifier),
    mpImpl(new MenuBarItemImpl) {}

MenuBarItem::~MenuBarItem()
{
    delete mpImpl;
}

void MenuBarItem::onMouseMove(const MouseEvent& evt)
{
}

void MenuBarItem::paint(DrawingContext& dc)
{
    if (!mpImpl->mpCategory || mpImpl->mpCategory->maName.empty())
        // Nothing to paint.
        return;

    ThemeDataGuard themeGuard(NULL, L"CompositedWindow::Window");
    HTHEME hTheme = themeGuard.get();
    if (!hTheme)
        return;

    LOGFONTW lgFont;
    FontGuard fontGuard(dc.mhDC, NULL);
    if (GetThemeSysFont(hTheme, TMT_CAPTIONFONT, &lgFont) == S_OK)
    {
        HFONT hFont = CreateFontIndirectW(&lgFont);
        fontGuard.set(hFont);
    }

    const Rect& rect = getRect();

    wchar_t str[64];
    if (!MultiByteToWideChar(CP_UTF8, 0, &mpImpl->mpCategory->maName[0], -1, str, 64))
        return;

    long x = rect.left + dc.maOffset.x;
    long y = rect.top + dc.maOffset.y;

    RECT rcPaint;
    rcPaint.left = x;
    rcPaint.top = y;
    rcPaint.right = x + rect.width();
    rcPaint.bottom = y + rect.height();
    DTTOPTS DttOpts = {sizeof(DTTOPTS)};
    DttOpts.dwFlags = DTT_COMPOSITED | DTT_GLOWSIZE;
    DttOpts.iGlowSize = 10;
    DrawThemeTextEx(
        hTheme, dc.mhDC, 0, 0, str, -1, DT_CENTER | DT_VCENTER,
        &rcPaint, &DttOpts);

    if (mpImpl->meState == Hovered)
    {
        cairo_t* cr = dc.mpCr;
        cairo_set_source_rgb(cr, 1, 1, 1);
        cairo_rectangle(cr, x, y, rect.width(), rect.height());
        cairo_stroke(cr);
    }
}

void MenuBarItem::init(const MenuCategory& cat)
{
    mpImpl->mpCategory = &cat;
}

void MenuBarItem::setState(State v)
{
    mpImpl->meState = v;
}

const MenuConfig::MenuItemsType* MenuBarItem::getMenuItems() const
{
    if (!mpImpl->mpCategory)
        return NULL;

    return &mpImpl->mpCategory->maItems;
}

}
