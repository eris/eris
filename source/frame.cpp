/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/frame.hpp"
#include "eris/topwindow.hpp"

#include <map>

#include <boost/thread/mutex.hpp>

namespace eris {

Frame::Frame() :
    mpWnd(NULL), mhWnd(NULL), mnCmdShow(0) {}

Frame::~Frame() {}

namespace {

typedef std::map<HWND, Frame*> FrameMapType;
FrameMapType winmap;
boost::mutex mtx;

}

Frame* Frame::getFrame(HWND hwnd)
{
    boost::mutex::scoped_lock lock(mtx);
    FrameMapType::iterator itr = winmap.find(hwnd);
    return itr == winmap.end() ? NULL : itr->second;
}

void Frame::setFrame(HWND hwnd, Frame* frame)
{
    if (!hwnd || !frame)
        return;

    boost::mutex::scoped_lock lock(mtx);
    winmap.insert(FrameMapType::value_type(hwnd, frame));
}

void Frame::removeFrame(HWND hwnd)
{
    if (!hwnd)
        return;

    boost::mutex::scoped_lock lock(mtx);
    winmap.erase(hwnd);
}

}
