/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/mainwindow.hpp"
#include "eris/frame.hpp"
#include "eris/window.hpp"
#include "eris/rect.hpp"
#include "eris/margins.hpp"
#include "eris/point.hpp"
#include "eris/mouseevent.hpp"
#include "eris/menupopup.hpp"
#include "eris/eris.hrc"
#include "eris/dlg/about.hpp"

#include "win32constants.hpp"

#include <windowsx.h>
#include <dwmapi.h>

namespace {

inline long width(const RECT& rc) { return rc.right - rc.left; }
inline long height(const RECT& rc) { return rc.bottom - rc.top; }

eris::MouseEvent toMouseEvent(WPARAM wParam, LPARAM lParam)
{
    eris::Point pos(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
    int code = static_cast<int>(wParam);
    return eris::MouseEvent(pos, code);
}

}

namespace wpMainWindow {

void handleCommand(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (LOWORD(wParam))
    {
        case ID_MENU_FILE_EXIT:
            PostMessage(hwnd, WM_CLOSE, 0, 0);
        break;
        case ID_MENU_HELP_ABOUT:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (frame)
            {
                eris::AboutDialog aDlg(frame);
                aDlg.execute();
            }
        }
        break;
        default:
            ;
    }
}

LRESULT CALLBACK appProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK dwmProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam, bool& callDWP);

LRESULT CALLBACK process(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    bool callAppProc = true;
    BOOL dwmEnabled = FALSE;
    LRESULT res = 0;
    HRESULT hr = DwmIsCompositionEnabled(&dwmEnabled);
    if (hr == S_OK && dwmEnabled)
    {
        res = dwmProc(hwnd, msg, wParam, lParam, callAppProc);
    }

    if (callAppProc)
    {
        res = appProc(hwnd, msg, wParam, lParam);
    }
    return res;
}

LRESULT CALLBACK appProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
        case WM_PAINT:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (frame)
                frame->mpWnd->onPaint();
        }
        break;
        case WM_SIZE:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            RECT rcWin;
            GetClientRect(hwnd, &rcWin);
            eris::Rect rect;
            rect.left = rcWin.left;
            rect.top = rcWin.top;
            rect.right = rcWin.right;
            rect.bottom = rcWin.bottom;
            frame->mpWnd->onResize(
                rect, static_cast<eris::TopWindow::ResizeType>(wParam));

            eris::Margins mgn = frame->mpWnd->getFrameMargins();

            // Invalidate the right and bottom frame borders to force them
            // to repaint because Windows doesn't repaint them when
            // shrinking.
            RECT rcRect;
            rcRect.left = rcWin.right - mgn.right;
            rcRect.top = rcWin.top + mgn.top;
            rcRect.right = rcWin.right;
            rcRect.bottom = rcWin.bottom;
            InvalidateRect(hwnd, &rcRect, true);

            rcRect.left = rcWin.left + mgn.left;
            rcRect.top = rcWin.bottom - mgn.bottom;
            InvalidateRect(hwnd, &rcRect, true);
        }
        break;
        case WM_MOUSEMOVE:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onMouseMove(toMouseEvent(wParam, lParam));
        }
        break;
        case WM_LBUTTONUP:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onLButtonUp(toMouseEvent(wParam, lParam));
        }
        break;
        case WM_LBUTTONDOWN:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onLButtonDown(toMouseEvent(wParam, lParam));
        }
        break;
        case WM_COMMAND:
            handleCommand(hwnd, msg, wParam, lParam);
        break;
        case WM_CLOSE:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (frame)
                frame->mpWnd->onClose();

            DestroyWindow(hwnd);
        }
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK dwmProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam, bool& callAppProc)
{
    LRESULT res = 0;
    callAppProc = !DwmDefWindowProc(hwnd, msg, wParam, lParam, &res);

    switch (msg)
    {
        case WM_SIZE:
        {
            RECT rcClient;
            GetWindowRect(hwnd, &rcClient);

            // Inform application of the frame change.
            SetWindowPos(hwnd,
                         NULL,
                         rcClient.left, rcClient.top,
                         width(rcClient), height(rcClient),
                         SWP_FRAMECHANGED);

            callAppProc = true;
        }
        break;
        case WM_ACTIVATE:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            eris::Margins mgn = frame->mpWnd->getFrameMargins();

            // Extend the frame into the client area.
            MARGINS margins = { -1, -1, -1, -1 };

            margins.cxLeftWidth = mgn.left;
            margins.cxRightWidth = mgn.right;
            margins.cyBottomHeight = mgn.bottom;
            margins.cyTopHeight = mgn.top;

            res = DwmExtendFrameIntoClientArea(hwnd, &margins);

            if (res != S_OK)
            {
                // Handle error.
            }

            callAppProc = true;
            res = 0;
        }
        break;
        case WM_PAINT:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (frame)
                frame->mpWnd->onFramePaint(eris::TopWindow::Restored);

            callAppProc = true;
            res = 0;
        }
        break;
        case WM_NCCALCSIZE:
        {
            if (wParam != TRUE)
                break;

            // Calculate new NCCALCSIZE_PARAMS based on custom NCA inset.
            NCCALCSIZE_PARAMS *pncsp = reinterpret_cast<NCCALCSIZE_PARAMS*>(lParam);

            pncsp->rgrc[0].left   = pncsp->rgrc[0].left   + 0;
            pncsp->rgrc[0].top    = pncsp->rgrc[0].top    + 0;
            pncsp->rgrc[0].right  = pncsp->rgrc[0].right  - 0;
            pncsp->rgrc[0].bottom = pncsp->rgrc[0].bottom - 0;

            res = 0;

            // No need to pass the message on to appProc.
            callAppProc = false;
        }
        break;
        case WM_NCHITTEST:
        {
            if (res != 0)
                break;

            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            // Handle hit testing in the NCA if not handled by dwmProc.
            eris::Point ptMouse(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
            res = frame->mpWnd->onHitTest(ptMouse);
            if (res != HTNOWHERE)
            {
                // Mouse is on the frame border for resizing.
                callAppProc = false;
                frame->mpWnd->onFrameBorder();
            }
        }
        break;
    }
    return res;
}

}

namespace wpFloatWindow {

LRESULT CALLBACK process(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
        case WM_SIZE:
        {
            RECT rcClient;
            GetWindowRect(hwnd, &rcClient);

            // Inform application of the frame change.
            SetWindowPos(hwnd,
                         NULL,
                         rcClient.left, rcClient.top,
                         width(rcClient), height(rcClient),
                         SWP_FRAMECHANGED);
        }
        break;
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            BeginPaint(hwnd, &ps);
            EndPaint(hwnd, &ps);
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (frame)
                frame->mpWnd->onPaint();
        }
        break;
        case WM_MOUSEMOVE:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onMouseMove(toMouseEvent(wParam, lParam));
        }
        break;
        case WM_LBUTTONUP:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onLButtonUp(toMouseEvent(wParam, lParam));
        }
        break;
        case WM_LBUTTONDOWN:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onLButtonDown(toMouseEvent(wParam, lParam));
        }
        break;
        case WM_CLOSE:
            DestroyWindow(hwnd);
        break;
        case WM_DESTROY:
            PostQuitMessage(0);
        break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

}

namespace wpMenuPopup {

LRESULT CALLBACK process(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    eris::MenuPopup::State* state =
        (eris::MenuPopup::State*)(GetWindowLongPtr(hwnd, GWLP_USERDATA));

    switch (msg)
    {
        case WM_CREATE:
        {
            // Associate the state structure passed via CreateWindowEx call
            // with this window handle.
            LPCREATESTRUCT pcs = reinterpret_cast<LPCREATESTRUCT>(lParam);
            SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LPARAM>(pcs->lpCreateParams));
            return 0;
        }
        case WM_PAINT:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (frame)
                frame->mpWnd->onPaint();
            return 0;
        }
        case WM_MOUSEMOVE:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onMouseMove(toMouseEvent(wParam, lParam));
        }
        break;
        case WM_LBUTTONDOWN:
        {
            eris::Frame* frame = eris::Frame::getFrame(hwnd);
            if (!frame)
                break;

            frame->mpWnd->onLButtonDown(toMouseEvent(wParam, lParam));
            state->mbDone = true;
        }
        break;
    }
    return DefWindowProc(hwnd, msg, wParam, lParam);
}

}

bool initApp(HINSTANCE hInstance)
{
    // main window
    WNDCLASSEX wc;
    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = 0;
    wc.lpfnWndProc   = wpMainWindow::process;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;

    wc.hIcon         = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(ID_RES_APPICON));
    wc.hIconSm       = reinterpret_cast<HICON>(
        LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(ID_RES_APPICON), IMAGE_ICON, 16, 16, 0));

    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = WNDCLS_MAINWINDOW;

    if (!RegisterClassEx(&wc))
        return false;

    wc.style = CS_DROPSHADOW;
    wc.lpszClassName = WNDCLS_FLOATWINDOW;
    wc.hIcon = NULL;
    wc.hIconSm = NULL;
    wc.lpfnWndProc = wpFloatWindow::process;

    if (!RegisterClassEx(&wc))
        return false;

    wc.lpszClassName = WNDCLS_MENUPOPUP;
    wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wc.lpfnWndProc = wpMenuPopup::process;
    if (!RegisterClassEx(&wc))
        return false;

    return true;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    if (!initApp(hInstance))
    {
        MessageBox(
            NULL, "Initialization Failed!", "Initialization Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    eris::Frame* frame = new eris::Frame;
    frame->mnCmdShow = nCmdShow;
    eris::MainWindow mainWnd(frame);
    mainWnd.init();
    DWORD style = WS_OVERLAPPEDWINDOW;
    DWORD xstyle = 0;
    return mainWnd.show(
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, style, xstyle, WNDCLS_MAINWINDOW, "Eris", NULL);
}

