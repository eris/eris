/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/mainwindow.hpp"

#include "eris/eris.hrc"
#include "eris/frame.hpp"
#include "eris/rect.hpp"
#include "eris/point.hpp"
#include "eris/margins.hpp"
#include "eris/mouseevent.hpp"
#include "eris/windows/scope_guard.hpp"
#include "eris/drawing_context.hpp"
#include "eris/menuconfig.hpp"

#include "win32constants.hpp"

// Windows specific headers.
#include <windowsx.h>
#include <dwmapi.h>
#include <vssym32.h>

#include <cairo-win32.h>

namespace eris {

namespace {

inline long width(const RECT& rc) { return rc.right - rc.left; }
inline long height(const RECT& rc) { return rc.bottom - rc.top; }

Rect getClientRect(const Rect& frameSize, const Margins& frameMargins)
{
    Rect rect;
    rect.left   = frameMargins.left;
    rect.top    = frameMargins.top;
    rect.right  = frameSize.right - frameMargins.right;
    rect.bottom = frameSize.bottom - frameMargins.bottom;
    return rect;
}

// Paint the title on the custom frame.
void paintTopNonClientArea(
    MainWindow* parent, HDC hdc, const Rect& area, const Margins& mgn, TopWindow::ResizeType resizeType)
{
    ThemeDataGuard themeGuard(NULL, L"CompositedWindow::Window");
    HTHEME hTheme = themeGuard.get();
    if (!hTheme)
        return;

    CompatibleDCGuard dcGuard(hdc);
    HDC hdcPaint = dcGuard.get();
    if (!hdcPaint)
        return;

    int cx = area.width();
    int cy = area.height();

    // Define the BITMAPINFO structure used to draw text. Note that biHeight
    // is negative. This is done because DrawThemeTextEx() needs the bitmap to
    // be in top-to-bottom order.
    BITMAPINFO dib = { 0 };
    dib.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
    dib.bmiHeader.biWidth       = cx;
    dib.bmiHeader.biHeight      = -cy;
    dib.bmiHeader.biPlanes      = 1;
    dib.bmiHeader.biBitCount    = 32;
    dib.bmiHeader.biCompression = BI_RGB;

    DIBSectionGuard dibGuard(hdc, hdcPaint, &dib, DIB_RGB_COLORS, NULL, NULL, 0);

    // Setup the theme drawing options.
    DTTOPTS DttOpts = {sizeof(DTTOPTS)};
    DttOpts.dwFlags = DTT_COMPOSITED | DTT_GLOWSIZE;
    DttOpts.iGlowSize = 10;

    // Select a font.
    LOGFONTW lgFont;
    FontGuard fontGuard(hdcPaint, NULL);
    if (GetThemeSysFont(hTheme, TMT_CAPTIONFONT, &lgFont) == S_OK)
    {
        HFONT hFont = CreateFontIndirectW(&lgFont);
        fontGuard.set(hFont);
    }

    // Draw the title in the center.
    RECT rcPaint;
    rcPaint.left = area.left;
    rcPaint.top = area.top;
    rcPaint.right = area.right;
    rcPaint.bottom = area.bottom;
    rcPaint.top += mgn.top;
    if (resizeType == eris::TopWindow::Maximized)
        rcPaint.top += 5;
    rcPaint.bottom = 50;
    DrawThemeTextEx(
        hTheme, hdcPaint, 0, 0, L"Important Document.ods  -  Project Eris", -1, DT_CENTER | DT_WORD_ELLIPSIS,
        &rcPaint, &DttOpts);

    {
        CairoContextGuard crGuard(hdcPaint);
        cairo_t* cr = crGuard.get();
        DrawingContext dc;
        dc.mpCr = cr;
        dc.mhDC = hdcPaint;
        parent->paintMenu(dc);
    }

    // Blit text to the frame.
    BitBlt(hdc, 0, 0, cx, cy, hdcPaint, 0, 0, SRCCOPY);
}

void paintClientArea(MainWindow* window, HDC hdc, const Rect& rcClient)
{
    // Paint the client area via double-buffering.
    CompatibleDCGuard compDCGuard(hdc);
    HDC hdcbuf = compDCGuard.get();

    CompatibleBitmapGuard bmGuard(hdc, hdcbuf, rcClient.width(), rcClient.height());
    CairoContextGuard crGuard(hdcbuf);
    cairo_t* cr = crGuard.get();

    DrawingContext dc;
    dc.mpCr = cr;
    dc.mhDC = hdcbuf;
    window->paintChildWindows(dc);

    // Now blit the object to the screen
    BitBlt(hdc, rcClient.left, rcClient.top, rcClient.width(), rcClient.height(), hdcbuf, 0, 0, SRCCOPY);
}

}

struct MainWindowImpl
{
    /**
     * Non-client area margins relative to the full window rectangle.
     */
    Margins maFrameMargins;

    /**
     * Paint margins for non-client area paint.  We shouldn't be drawing
     * anything within these margins.
     */
    Margins maNCPaintMargins;

    /**
     * Menu configuration that stores the logical structure of the toolbar
     * menus.
     */
    MenuConfig maMenuConfig;

    MainWindowImpl() :
        maFrameMargins(8, 100, 8, 22), // left, top, right, bottom
        maNCPaintMargins(0, 0, 0, 0)
    {}

    ~MainWindowImpl() {}
};

MainWindow::MainWindow(Frame* frame) :
    TopWindow(frame, 0),
    maMenu(frame, ID_MAIN_MENU),
    maSheet(this, ID_SHEET_GRID),
    mpImpl(new MainWindowImpl)
{
}

MainWindow::~MainWindow()
{
    delete mpImpl;
}

void MainWindow::onResize(const Rect& frameRect, ResizeType type)
{
    // Top-level window has the full size of the frame.
    setRect(frameRect);

    // Calculate the paint margins.  We shouldn't paint anything within these
    // margins.
    RECT rcFrame = {0,0,0,0};
    AdjustWindowRectEx(&rcFrame, WS_OVERLAPPEDWINDOW & ~WS_CAPTION, FALSE, NULL);
    Margins& mgn = mpImpl->maNCPaintMargins;
    mgn.left = -rcFrame.left;
    mgn.top = -rcFrame.top;
    mgn.right = rcFrame.right;
    mgn.bottom = rcFrame.bottom;

    // all window items in the non-client area is with reference to the parent
    // window coordinate.
    Rect rectWin = getRect();
    maMenu.setRect(
        Rect(rectWin.left + mgn.left + 75, rectWin.top + mgn.top+ 20,
             rectWin.right - mgn.right, rectWin.top + mgn.top + 40));
    maMenu.resizeItems();

    // all window items in the client area is with reference to the top-left
    // corner of the client area.
    Rect rect = getClientRect(frameRect, mpImpl->maFrameMargins);
    maSheet.setRect(Rect(0, 0, rect.width(), rect.height()));

    HWND hwnd = getFrame()->mhWnd;
    DCGuard dcGuard(hwnd);
    HDC hdc = dcGuard.get();
    Rect area = frameRect;
    area.bottom = area.top + mpImpl->maFrameMargins.top;
    paintTopNonClientArea(this, hdc, area, mpImpl->maNCPaintMargins, type);
    Rect rcClient = getClientRect(getRect(), mpImpl->maFrameMargins);
    paintClientArea(this, hdc, rcClient);
}

void MainWindow::onPaint()
{
    Frame* frame = getFrame();
    if (!frame)
        return;

    PaintGuard dcGuard(frame->mhWnd);
    HDC hdc = dcGuard.get();
    paintClientArea(this, hdc, getClientRect(getRect(), mpImpl->maFrameMargins));
}

void MainWindow::onFramePaint(ResizeType type)
{
    Frame* frame = getFrame();
    if (!frame)
        return;

    HWND hwnd = frame->mhWnd;
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hwnd, &ps);
    Rect area = getRect();
    area.bottom = area.top + mpImpl->maFrameMargins.top;
    paintTopNonClientArea(this, hdc, area, mpImpl->maNCPaintMargins, type);
    paintClientArea(this, hdc, getClientRect(getRect(), mpImpl->maFrameMargins));
    EndPaint(hwnd, &ps);
}

int MainWindow::onHitTest(const Point& pt)
{
    // Mouse pointer is relative to screen.

    RECT rcWindow;
    GetWindowRect(getFrame()->mhWnd, &rcWindow);

    // Check if the mouse is within the menu bar.
    Rect rcMenu = maMenu.getRect();
    rcMenu.move(rcWindow.left, rcWindow.top);
    if (rcMenu.contains(pt))
        return HTNOWHERE;

    // Determine if the hit test is for resizing. Default middle (1,1).
    size_t nRow = 1;
    size_t nCol = 1;
    bool bOnResizeBorder = false;
    const Margins& mgn = mpImpl->maFrameMargins;

    // Determine if the point is at the top or bottom of the window.
    if (pt.y >= rcWindow.top && pt.y < rcWindow.top + mgn.top)
    {
        bOnResizeBorder = (pt.y < (rcWindow.top + mpImpl->maNCPaintMargins.top));
        nRow = 0;
    }
    else if (pt.y < rcWindow.bottom && pt.y >= rcWindow.bottom - mgn.bottom)
    {
        nRow = 2;
    }

    // Determine if the point is at the left or right of the window.
    if (pt.x >= rcWindow.left && pt.x < rcWindow.left + mgn.left)
    {
        nCol = 0; // left side
    }
    else if (pt.x < rcWindow.right && pt.x >= rcWindow.right - mgn.right)
    {
        nCol = 2; // right side
    }

    // Hit test (HTTOPLEFT, ... HTBOTTOMRIGHT)
    LRESULT hitTests[3][3] =
    {
        { HTTOPLEFT, bOnResizeBorder ? HTTOP : HTCAPTION, HTTOPRIGHT },
        { HTLEFT, HTNOWHERE, HTRIGHT },
        { HTBOTTOMLEFT, HTBOTTOM, HTBOTTOMRIGHT },
    };

    return static_cast<int>(hitTests[nRow][nCol]);
}

void MainWindow::onFrameBorder()
{
    maMenu.reset();
}

void MainWindow::onClose()
{
}

void MainWindow::onMouseMove(const MouseEvent& evt)
{
    if (maMenu.getRect().contains(evt.getPos()))
        // Mouse cursor is over the menu bar.
        maMenu.onMouseMove(evt);
    else
        maMenu.reset();
}

void MainWindow::onLButtonDown(const MouseEvent& evt)
{
    if (maMenu.getRect().contains(evt.getPos()))
        // Mouse cursor is over the menu bar.
        maMenu.onLButtonDown(evt);
}

void MainWindow::onLButtonUp(const MouseEvent& evt)
{
    if (maMenu.getRect().contains(evt.getPos()))
        // Mouse cursor is over the menu bar.
        maMenu.onLButtonUp(evt);
}

Margins MainWindow::getFrameMargins() const
{
    return mpImpl->maFrameMargins;
}

void MainWindow::init()
{
    // Initialize menus.
    mpImpl->maMenuConfig.init();

    maMenu.init(mpImpl->maMenuConfig);
    maSheet.init();
}

void MainWindow::paintMenu(DrawingContext& dc)
{
    const Margins& mgn = mpImpl->maNCPaintMargins;
    cairo_t* cr = dc.mpCr;
    cairo_surface_t* image = cairo_image_surface_create_from_png ("key-64.png");
    cairo_set_source_surface (cr, image, mgn.left, mgn.top);
    cairo_paint (cr);
    cairo_surface_destroy(image);

    maMenu.paint(dc);
}

void MainWindow::paintChildWindows(DrawingContext& dc)
{
    for (Window* child = firstChild(); child; child = nextChild())
        child->paint(dc);
}

}
