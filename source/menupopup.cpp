/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/menupopup.hpp"
#include "eris/frame.hpp"
#include "eris/rect.hpp"
#include "eris/windows/scope_guard.hpp"
#include "eris/mouseevent.hpp"
#include "win32constants.hpp"

#include <windows.h>
#include <vsstyle.h>
#include <vssym32.h>
#include <uxtheme.h>

namespace eris {

namespace {

const int topMargin = 3;
const int horItemMargin = 3; // left and right margins
const int horGutterItemGap = 3; // gap between gutter bar and menu item text.
const double verItemMarginRatio = 1.7; // item hight = text height * this value
const int gutterWidth = 35;

size_t NOT_SELECTED = 9999;

}

struct MenuPopupImpl
{
    Frame* mpParent;
    const MenuConfig::MenuItemsType* mpItems;
    size_t mnSelectedItem;
    long mnTextHeight;

    MenuPopupImpl() :
        mnSelectedItem(NOT_SELECTED),
        mnTextHeight(-1) {}
};

MenuPopup::State::State() :
    mbDone(false) {}

MenuPopup::MenuPopup(Frame* parent) :
    TopWindow(9999),
    mpImpl(new MenuPopupImpl)
{
    mpImpl->mpParent = parent;
    getFrame()->mnCmdShow = SW_SHOWNA;
}

MenuPopup::~MenuPopup()
{
    delete mpImpl;
}

void MenuPopup::onResize(const Rect& frameRect, ResizeType type)
{
}

namespace {

void paintMenuItem(HTHEME hTheme, HDC hdc, const Rect& rect, const MenuItem& item, bool highlight)
{
    RECT rcBack;
    rcBack.left = 0;
    rcBack.right = rect.width();
    rcBack.top = 0;
    rcBack.bottom = rect.height();

    RECT rcGutter = rcBack;
    rcGutter.right = rcBack.left + gutterWidth;

    RECT rcHighlight = rcBack;
    rcHighlight.left += horItemMargin;
    rcHighlight.right -= horItemMargin;

    RECT rcText = rcBack;
    rcText.left = rcGutter.right + horGutterItemGap;

    int state = MPI_HOT;

    DrawThemeBackground(hTheme, hdc, MENU_POPUPBACKGROUND, state, &rcBack, NULL);
    DrawThemeBackground(hTheme, hdc, MENU_POPUPGUTTER, state, &rcGutter, NULL);
    if (highlight)
        DrawThemeBackground(hTheme, hdc, MENU_POPUPITEM, state, &rcHighlight, NULL);

    wchar_t str[64];
    if (!MultiByteToWideChar(CP_UTF8, 0, &item.maName[0], -1, str, 64))
        return;

    GetThemeBackgroundContentRect(
        hTheme, hdc, MENU_POPUPITEM, state, &rcText, &rcText);

    DrawThemeTextEx(
        hTheme, hdc, MENU_POPUPITEM, state, str, -1,
        DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_WORD_ELLIPSIS, &rcText, NULL);
}

size_t getSelectedItem(
    const MenuPopup& wnd, const Point& pt, const MenuConfig::MenuItemsType& items,
    long itemHeight)
{
    Rect rectWnd = wnd.getRect();
    rectWnd.move(-rectWnd.left, -rectWnd.top);  // Make the top-left corner (0, 0).
    if (!rectWnd.contains(pt))
        return NOT_SELECTED;

    long bottom = topMargin + itemHeight;
    for (size_t i = 0, n = items.size(); i < n; ++i)
    {
        if (pt.y <= bottom)
            return i;

        bottom += itemHeight;
    }

    return NOT_SELECTED;
}

}

void MenuPopup::onPaint()
{
    if (!mpImpl->mpItems)
        return;

    const MenuConfig::MenuItemsType& items = *mpImpl->mpItems;
    const Rect& rect = getRect();

    ThemeDataGuard themeGuard(getFrame()->mhWnd, VSCLASS_MENU);
    HTHEME hTheme = themeGuard.get();
    if (!hTheme)
        return;

    PaintGuard paintGuard(getFrame()->mhWnd);
    HDC hdc = paintGuard.get();

    CompatibleDCGuard compDCGuard(hdc);
    HDC hdcbuf = compDCGuard.get();

    CompatibleBitmapGuard bmGuard(hdc, hdcbuf, rect.width(), rect.height());

    // Select a font.
    LOGFONTW lgFont;
    FontGuard fontGuard(hdcbuf, NULL);
    if (GetThemeSysFont(hTheme, TMT_MENUFONT, &lgFont) == S_OK)
    {
        HFONT hFont = CreateFontIndirectW(&lgFont);
        fontGuard.set(hFont);
    }

    // Draw the background first.
    TEXTMETRICW tm;
    GetThemeTextMetrics(hTheme, hdcbuf, MENU_POPUPITEM, MPI_NORMAL, &tm);
    mpImpl->mnTextHeight = tm.tmHeight;

    RECT rc, rcGutter;
    rc.left = 0;
    rc.top = 0;
    rc.right = rect.width();
    rc.bottom = rect.height();

    rcGutter = rc;
    rcGutter.right = rc.left + gutterWidth;

    int state = MPI_HOT;
    DrawThemeBackground(hTheme, hdcbuf, MENU_POPUPBACKGROUND, state, &rc, NULL);
    DrawThemeBackground(hTheme, hdcbuf, MENU_POPUPGUTTER, state, &rcGutter, NULL);

    RECT rcText;
    rcText.left = rcGutter.right + horGutterItemGap;
    rcText.top = rc.top + topMargin;
    rcText.right = rc.right - horItemMargin;
    rcText.bottom = rcText.top + static_cast<long>(tm.tmHeight*verItemMarginRatio);

    RECT rcHighlight = rcText;
    rcHighlight.left = rc.left + horItemMargin;

    long textHeight = rcText.bottom - rcText.top;
    long highlightHeight = rcHighlight.bottom - rcHighlight.top;

    MenuConfig::MenuItemsType::const_iterator it = items.begin(), itEnd = items.end();
    for (; it != itEnd; ++it)
    {
        wchar_t str[64];
        if (!MultiByteToWideChar(CP_UTF8, 0, &it->maName[0], -1, str, 64))
            return;

        RECT rcTextCont;
        GetThemeBackgroundContentRect(
            hTheme, hdcbuf, MENU_POPUPITEM, state, &rcText, &rcTextCont);

        DrawThemeTextEx(
            hTheme, hdcbuf, MENU_POPUPITEM, state, str, -1,
            DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_WORD_ELLIPSIS, &rcTextCont, NULL);

        rcText.top += textHeight;
        rcText.bottom += textHeight;
        rcHighlight.top += highlightHeight;
        rcHighlight.bottom += highlightHeight;
    }

    BitBlt(hdc, 0, 0, rect.width(), rect.height(), hdcbuf, 0, 0, SRCCOPY);
}

void MenuPopup::onFramePaint(ResizeType /*type*/) {}

void MenuPopup::onMouseMove(const MouseEvent& evt)
{
    if (!mpImpl->mpItems)
        return;

    if (mpImpl->mnTextHeight < 0)
        return;

    const MenuConfig::MenuItemsType& items = *mpImpl->mpItems;
    long itemHeight = mpImpl->mnTextHeight*verItemMarginRatio;

    // Determine which menu item is hovered over.
    size_t selected = getSelectedItem(*this, evt.getPos(), items, itemHeight);

    if (mpImpl->mnSelectedItem == selected)
        return;

    // Highlight selected menu and dehighlight the previously selected.
    Rect rect = getRect();
    rect.top = 0;
    rect.bottom = itemHeight;

    ThemeDataGuard themeGuard(getFrame()->mhWnd, VSCLASS_MENU);
    HTHEME hTheme = themeGuard.get();
    if (!hTheme)
        return;

    DCGuard paintGuard(getFrame()->mhWnd);
    HDC hdc = paintGuard.get();

    CompatibleDCGuard compDCGuard(hdc);
    HDC hdcbuf = compDCGuard.get();

    CompatibleBitmapGuard bmGuard(hdc, hdcbuf, rect.width(), rect.height());

    // Select a font.
    LOGFONTW lgFont;
    FontGuard fontGuard(hdcbuf, NULL);
    if (GetThemeSysFont(hTheme, TMT_MENUFONT, &lgFont) == S_OK)
    {
        HFONT hFont = CreateFontIndirectW(&lgFont);
        fontGuard.set(hFont);
    }

    // De-highlight previously selected item.
    size_t pos = mpImpl->mnSelectedItem;
    if (pos != NOT_SELECTED)
    {
        paintMenuItem(hTheme, hdcbuf, rect, items[pos], false);
        long topPos = topMargin + rect.height()*static_cast<long>(pos);
        BitBlt(hdc, 0, topPos, rect.width(), rect.height(), hdcbuf, 0, 0, SRCCOPY);
    }

    // Highlight new item.
    pos = selected;
    if (pos != NOT_SELECTED)
    {
        paintMenuItem(hTheme, hdcbuf, rect, items[pos], true);
        long topPos = topMargin + rect.height()*static_cast<long>(pos);
        BitBlt(hdc, 0, topPos, rect.width(), rect.height(), hdcbuf, 0, 0, SRCCOPY);
    }

    mpImpl->mnSelectedItem = selected;
}

void MenuPopup::onLButtonDown(const MouseEvent& evt)
{
    if (!mpImpl->mpItems)
        return;

    if (mpImpl->mnTextHeight < 0)
        return;

    const MenuConfig::MenuItemsType& items = *mpImpl->mpItems;
    long itemHeight = mpImpl->mnTextHeight*verItemMarginRatio;

    // Determine which menu item is clicked on.
    mpImpl->mnSelectedItem = getSelectedItem(*this, evt.getPos(), items, itemHeight);
}

namespace {

bool popupDone(const MenuPopup::State& state, HWND hwndPopup, HWND hwndOwner)
{
    if (state.mbDone)
        return true;

    // The parent window is no longer the active window i.e. the user most
    // likely has switched to another window via alt-tab.
    HWND hwndActive = GetActiveWindow();
    if (hwndActive != hwndOwner && !IsChild(hwndActive, hwndOwner))
        return true;

    if (GetCapture() != hwndOwner)
        return true;

    return false;
}

}

void MenuPopup::beginPopup()
{
    Frame* frame = getFrame();
    const Rect& rect = getRect();

    DWORD style = WS_POPUP;
    DWORD xstyle = WS_EX_TOOLWINDOW | WS_EX_TOPMOST;

    State state;
    HWND hwndPopup = CreateWindowEx(
        xstyle,
        WNDCLS_MENUPOPUP, "",
        style, rect.left, rect.top, rect.width(), rect.height(),
        mpImpl->mpParent->mhWnd, NULL, GetModuleHandle(NULL), &state);

    if (!hwndPopup)
    {
        MessageBox(NULL, "Window Creation Failed!", "Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return;
    }

    frame->mhWnd = hwndPopup;
    Frame::setFrame(frame->mhWnd, frame);

    ShowWindow(hwndPopup, SW_SHOWNOACTIVATE);
    SetCapture(mpImpl->mpParent->mhWnd);

    HWND hwndOwner = mpImpl->mpParent->mhWnd;
    MSG msg;
    POINT pt;
    while (GetMessage(&msg, NULL, 0, 0) > 0)
    {
        if (popupDone(state, hwndPopup, hwndOwner))
            break;

        // At this point, we get to snoop at all input messages before
        // they get dispatched.  This allows us to route all input to our
        // popup window even if really belongs to somebody else.

        // All mouse messages are remunged and directed at our popup
        // menu. If the mouse message arrives as client coordinates, then
        // we have to convert it from the client coordinates of the original
        // target to the client coordinates of the new target.
        switch (msg.message)
        {
            // These mouse messages arrive in client coordinates, so in
            // addition to stealing the message, we also need to convert the
            // coordinates.
            case WM_MOUSEMOVE:
            case WM_LBUTTONDOWN:
            case WM_LBUTTONUP:
            case WM_LBUTTONDBLCLK:
            case WM_RBUTTONDOWN:
            case WM_RBUTTONUP:
            case WM_RBUTTONDBLCLK:
            case WM_MBUTTONDOWN:
            case WM_MBUTTONUP:
            case WM_MBUTTONDBLCLK:
                pt.x = (short)LOWORD(msg.lParam);
                pt.y = (short)HIWORD(msg.lParam);
                MapWindowPoints(msg.hwnd, hwndPopup, &pt, 1);
                msg.lParam = MAKELPARAM(pt.x, pt.y);
                msg.hwnd = hwndPopup;
                break;

            // These mouse messages arrive in screen coordinates, so we just
            // need to steal the message.
            case WM_NCMOUSEMOVE:
            case WM_NCLBUTTONDOWN:
            case WM_NCLBUTTONUP:
            case WM_NCLBUTTONDBLCLK:
            case WM_NCRBUTTONDOWN:
            case WM_NCRBUTTONUP:
            case WM_NCRBUTTONDBLCLK:
            case WM_NCMBUTTONDOWN:
            case WM_NCMBUTTONUP:
            case WM_NCMBUTTONDBLCLK:
                msg.hwnd = hwndPopup;
                break;

            // We need to steal all keyboard messages, too.
            case WM_KEYDOWN:
            case WM_KEYUP:
            case WM_CHAR:
            case WM_DEADCHAR:
            case WM_SYSKEYDOWN:
            case WM_SYSKEYUP:
            case WM_SYSCHAR:
            case WM_SYSDEADCHAR:
                msg.hwnd = hwndPopup;
                break;
        }

        TranslateMessage(&msg);
        DispatchMessage(&msg);

        if (popupDone(state, hwndPopup, hwndOwner))
            break;
    }

    // Clean up the capture we created.
    ReleaseCapture();
    DestroyWindow(hwndPopup);
    Frame::removeFrame(hwndPopup);
    frame->mhWnd = NULL;

    // If we got a WM_QUIT message, then re-post it so the caller's message
    // loop will see it.
    if (msg.message == WM_QUIT)
    {
        PostQuitMessage((int)msg.wParam);
        return;
    }

    if (!mpImpl->mpItems)
        return;

    const MenuConfig::MenuItemsType& items = *mpImpl->mpItems;
    if (mpImpl->mnSelectedItem == NOT_SELECTED || mpImpl->mnSelectedItem >= items.size())
        // No item is clicked on.
        return;

    const MenuItem& item = items[mpImpl->mnSelectedItem];
    if (item.meType != MenuItem::Event)
        return;

    // Dispatch the command.
    HWND hwnd = mpImpl->mpParent->mhWnd;
    PostMessage(hwnd, WM_COMMAND, item.mnEventID, 0);
}

void MenuPopup::endPopup()
{
    terminate();
}

void MenuPopup::init(const MenuConfig::MenuItemsType* items)
{
    mpImpl->mpItems = items;
}

}
