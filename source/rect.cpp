/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/rect.hpp"
#include "eris/point.hpp"

namespace eris {

Rect::Rect() : left(0), top(0), right(0), bottom(0) {}
Rect::Rect(const Rect& r) : left(r.left), top(r.top), right(r.right), bottom(r.bottom) {}
Rect::Rect(long _left, long _top, long _right, long _bottom) :
    left(_left), top(_top), right(_right), bottom(_bottom) {}

void Rect::reset(long _left, long _top, long _right, long _bottom)
{
    left = _left;
    top = _top;
    right = _right;
    bottom = _bottom;
}

void Rect::move(long x, long y)
{
    left += x;
    right += x;
    top += y;
    bottom += y;
}

long Rect::width() const
{
    return right - left;
}

long Rect::height() const
{
    return bottom - top;
}

bool Rect::contains(const Point& pt) const
{
    return left <= pt.x && pt.x <= right && top <= pt.y && pt.y <= bottom;
}

}
