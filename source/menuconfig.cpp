/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/menuconfig.hpp"
#include "eris/eris.hrc"

namespace eris {

struct MenuConfigImpl
{
    boost::ptr_vector<MenuCategory> maCategories;

    void init()
    {
        maCategories.push_back(new MenuCategory("File"));
        {
            MenuCategory& cat = maCategories.back();
            cat.maItems.push_back(new MenuItem("About", ID_MENU_HELP_ABOUT));
            cat.maItems.push_back(new MenuItem("Exit", ID_MENU_FILE_EXIT));
        }

        maCategories.push_back(new MenuCategory("Edit"));
        {
            MenuCategory& cat = maCategories.back();
            cat.maItems.push_back(new MenuItem("Test 1", 0));
            cat.maItems.push_back(new MenuItem("Test 2", 0));
            cat.maItems.push_back(new MenuItem("Test 3", 0));
            cat.maItems.push_back(new MenuItem("Test 4", 0));
            cat.maItems.push_back(new MenuItem("Test 5", 0));
            cat.maItems.push_back(new MenuItem("Test 6", 0));
        }

        maCategories.push_back(new MenuCategory("View"));
        {
            MenuCategory& cat = maCategories.back();
            cat.maItems.push_back(new MenuItem("Test 1", 0));
            cat.maItems.push_back(new MenuItem("Test 2", 0));
            cat.maItems.push_back(new MenuItem("Test 3", 0));
            cat.maItems.push_back(new MenuItem("Test 4", 0));
            cat.maItems.push_back(new MenuItem("Test 5", 0));
            cat.maItems.push_back(new MenuItem("Test 6", 0));
        }
    }
};

MenuConfig::MenuConfig() :
    mpImpl(new MenuConfigImpl)
{
}

MenuConfig::~MenuConfig()
{
    delete mpImpl;
}

void MenuConfig::init()
{
    mpImpl->init();
}

const MenuConfig::TopCategoriesType& MenuConfig::getTopCategories() const
{
    return mpImpl->maCategories;
}

MenuCategory::MenuCategory(const std::string& name) : maName(name) {}

MenuItem::MenuItem(const std::string& name, int eventID) :
    meType(Event), maName(name), mnEventID(eventID) {}

MenuItem::MenuItem(const std::string& name, MenuCategory* p) :
    meType(SubCategory), maName(name), mpSubCategory(p) {}

MenuItem::~MenuItem()
{
    if (meType == SubCategory)
        delete mpSubCategory;
}

}
