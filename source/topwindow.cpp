/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#include "eris/topwindow.hpp"
#include "eris/frame.hpp"
#include "eris/margins.hpp"

#include <windows.h>

namespace eris {

struct TopWindowImpl
{
    Frame* mpFrame;
    TopWindowImpl(Frame* frame) : mpFrame(frame) {}
    ~TopWindowImpl()
    {
        delete mpFrame;
    }
};

TopWindow::TopWindow(int identifier) :
    Window(NULL, identifier),
    mpImpl(new TopWindowImpl(new Frame))
{
    mpImpl->mpFrame->mpWnd = this;
}

TopWindow::TopWindow(Frame* frame, int identifier) :
    Window(NULL, identifier),
    mpImpl(new TopWindowImpl(frame))
{
    mpImpl->mpFrame->mpWnd = this;
}

TopWindow::~TopWindow()
{
    Frame::removeFrame(mpImpl->mpFrame->mhWnd);
    delete mpImpl;
}

int TopWindow::onHitTest(const Point&) { return HTNOWHERE; }

void TopWindow::onFrameBorder() {}
void TopWindow::onClose() {}

Margins TopWindow::getFrameMargins() const { return Margins(); }

int TopWindow::show(int x, int y, int w, int h, int flag, int exflag, const char* cls, const char* title, Frame* parent)
{
    HWND hParentWnd = NULL;
    if (parent)
        hParentWnd = parent->mhWnd;

    getFrame()->mhWnd = CreateWindowEx(
        exflag,
        cls, title,
        flag, x, y, w, h,
        hParentWnd, NULL, GetModuleHandle(NULL), NULL);

    if (!mpImpl->mpFrame->mhWnd)
    {
        MessageBox(NULL, "Window Creation Failed!", "Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    Frame::setFrame(mpImpl->mpFrame->mhWnd, mpImpl->mpFrame);

    ShowWindow(mpImpl->mpFrame->mhWnd, mpImpl->mpFrame->mnCmdShow);
    UpdateWindow(mpImpl->mpFrame->mhWnd);

    // Message loop.
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return static_cast<int>(msg.wParam);
}

void TopWindow::terminate()
{
    SendMessage(mpImpl->mpFrame->mhWnd, WM_CLOSE, 0, 0);
}

Frame* TopWindow::getFrame()
{
    return mpImpl->mpFrame;
}

}
