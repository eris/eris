/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#ifndef __ERIS_MENUPOPUP_HPP__
#define __ERIS_MENUPOPUP_HPP__

#include "eris/topwindow.hpp"
#include "eris/menuconfig.hpp"

namespace eris {

struct Frame;
struct MenuPopupImpl;
struct MenuCateogry;

class MenuPopup : public TopWindow
{
    MenuPopupImpl* mpImpl;
public:
    struct State
    {
        bool mbDone;
        State();
    };

    MenuPopup(Frame* parent);
    ~MenuPopup();

    virtual void onResize(const Rect& frameRect, ResizeType type);
    virtual void onPaint();
    virtual void onFramePaint(ResizeType type);

    virtual void onMouseMove(const MouseEvent& evt);
    virtual void onLButtonDown(const MouseEvent& evt);

    void beginPopup();
    void endPopup();

    void init(const MenuConfig::MenuItemsType* items);
};

}

#endif
