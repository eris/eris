/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#ifndef __ERIS_SCOPE_GUARD_HPP__
#define __ERIS_SCOPE_GUARD_HPP__

#include <windows.h>
#include <uxtheme.h>
#include <cairo-win32.h>

namespace eris {

class PaintGuard
{
    HWND mhWnd;
    PAINTSTRUCT mPS;
    HDC mhDC;
public:
    PaintGuard(HWND hwnd);
    ~PaintGuard();

    HDC get();
};

class DCGuard
{
    HWND mhWnd;
    HDC mhDC;
public:
    DCGuard(HWND hwnd);
    ~DCGuard();

    HDC get();
};

class CompatibleDCGuard
{
    HDC mhOriginalDC;
    HDC mhNewDC;
public:
    CompatibleDCGuard(HDC hdc);
    ~CompatibleDCGuard();

    HDC get();
};

class ThemeDataGuard
{
    HTHEME mhTheme;
public:
    ThemeDataGuard(HWND hwnd, LPCWSTR clsList);
    ~ThemeDataGuard();

    HTHEME get();
};

class DIBSectionGuard
{
    HBITMAP mhDIB;
    HBITMAP mhDIBOld;
    HDC mhDCCompat;
public:
    DIBSectionGuard(
        HDC hdc, HDC hdcCompat, const BITMAPINFO *pbmi, UINT iUsage, VOID **ppvBits,
        HANDLE hSection, DWORD dwOffset);
    ~DIBSectionGuard();

    HBITMAP get();
};

class FontGuard
{
    HDC mhDC;
    HFONT mhFontOld;
public:
    FontGuard(HDC hdc, HFONT font);
    ~FontGuard();

    void set(HFONT font);
};

class CompatibleBitmapGuard
{
    HBITMAP mhBuf;
public:
    CompatibleBitmapGuard(HDC hdc, HDC hdcbuf, int width, int height);
    ~CompatibleBitmapGuard();
};

class CairoContextGuard
{
    cairo_surface_t* mpSurface;
    cairo_t* mpCr;
public:
    CairoContextGuard(HDC hdc);
    ~CairoContextGuard();

    cairo_t* get();
};

}

#endif
