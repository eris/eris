/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#ifndef __ERIS_MENUBAR_HPP__
#define __ERIS_MENUBAR_HPP__

#include "eris/window.hpp"
#include "eris/menuconfig.hpp"

#include <string>

namespace eris {

struct Frame;
struct Rect;
struct MenuBarImpl;
struct MenuBarItemImpl;

/**
 * The entire rebar that houses individual top-level menu items.
 */
class MenuBar : public Window
{
    MenuBarImpl* mpImpl;
public:
    MenuBar(Frame* frame, int identifier);
    virtual ~MenuBar();

    virtual void onMouseMove(const MouseEvent& evt);
    virtual void onLButtonDown(const MouseEvent& evt);
    virtual void onLButtonUp(const MouseEvent& evt);
    virtual void paint(DrawingContext& dc);

    void init(const MenuConfig& config);

    /**
     * Re-calculate the rectangle of each menu bar item based on the rectangle
     * of the menu bar.
     */
    void resizeItems();

    void reset();
    void terminate();
protected:
    virtual Frame* getFrame();
};

/**
 * Individual top-level menu item in the rebar at the top.
 */
class MenuBarItem : public Window
{
    MenuBarItemImpl* mpImpl;
public:
    enum State { Normal = 0, Hovered = 1 };
    MenuBarItem(Window* parent, int identifier);
    virtual ~MenuBarItem();

    virtual void onMouseMove(const MouseEvent& evt);

    virtual void paint(DrawingContext& dc);

    void init(const MenuCategory& cat);
    void setState(State v);
    const MenuConfig::MenuItemsType* getMenuItems() const;
};

}

#endif
