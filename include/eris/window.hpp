/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#ifndef __ERIS_WINDOW_HPP__
#define __ERIS_WINDOW_HPP__

#include <boost/noncopyable.hpp>

namespace eris {

class Window;
struct Frame;
struct Rect;
struct DrawingContext;
struct WindowImpl;
class MouseEvent;

class Window : boost::noncopyable
{
public:
    Window(Window* parent, int identifier);
    virtual ~Window();

    virtual void onMouseMove(const MouseEvent& evt);
    virtual void onLButtonDown(const MouseEvent& evt);
    virtual void onLButtonUp(const MouseEvent& evt);

    virtual void paint(DrawingContext& dc);

    void setRect(const Rect& rect);
    Rect getRect() const;

protected:
    virtual Frame* getFrame();

    Window* getParent();
    Window* firstChild();
    Window* nextChild();
    int getIdentifier() const;

private:
    void addChild(Window* wnd);

private:
    WindowImpl* mpImpl;
};

}

#endif
