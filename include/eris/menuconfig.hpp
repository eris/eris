/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#ifndef __ERIS_MENUCONFIG_HPP__
#define __ERIS_MENUCONFIG_HPP__

#include <string>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/noncopyable.hpp>

namespace eris {

struct MenuItem;
struct MenuCategory;

struct MenuConfigImpl;

class MenuConfig : boost::noncopyable
{
    MenuConfigImpl* mpImpl;
public:
    typedef boost::ptr_vector<MenuCategory> TopCategoriesType;
    typedef boost::ptr_vector<MenuItem> MenuItemsType;

    MenuConfig();
    ~MenuConfig();

    const TopCategoriesType& getTopCategories() const;

    void init();
};

/**
 * Top-level menu category or a sub-category.
 */
struct MenuCategory
{
    std::string maName;
    MenuConfig::MenuItemsType maItems;

    MenuCategory(const std::string& name);
};

/**
 * Meta-data for individual menu item.
 */
struct MenuItem
{
    enum Type { Event = 0, SubCategory = 1 };
    Type meType;
    std::string maName;
    union {
        int mnEventID;
        MenuCategory* mpSubCategory;
    };

    MenuItem(const std::string& name, int eventID);
    MenuItem(const std::string& name, MenuCategory* p);
    ~MenuItem();

private:
    MenuItem(); // disabled
};

}

#endif
