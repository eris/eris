/***************************************************************************
 *
 * Copyright (c) 2011 Kohei Yoshida.
 *
 * Version: MPL 1.1+ / GPLv3+ / LGPLv3+
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 or newer (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 3 or later (the "GPLv3+"),
 * or the GNU Lesser General Public License Version 3 or later (the
 * "LGPLv3+"), in which case the provisions of the GPLv3+ or the LGPLv3+ are
 * applicable instead of those above.
 *
 ***************************************************************************/

#ifndef __ERIS_TOPWINDOW_HPP__
#define __ERIS_TOPWINDOW_HPP__

#include "eris/window.hpp"

namespace eris {

struct Frame;
struct Rect;
struct Margins;
struct Point;
struct TopWindowImpl;

/**
 * Top window always has an associated frame instance.
 */
class TopWindow : public Window
{
public:
    enum ResizeType { Restored = 0, Minimized = 1, Maximized = 2, MaxShow = 3, MaxHide = 4 };

    TopWindow(int identifier);
    TopWindow(Frame* frame, int identifier);
    ~TopWindow();

    virtual void onResize(const Rect& frameRect, ResizeType type) = 0;
    virtual void onPaint() = 0;
    virtual void onFramePaint(ResizeType type) = 0;
    virtual int onHitTest(const Point& pt);

    /**
     * Mouse pointer is positioned on the frame border for moving or resizing
     * of the window.
     */
    virtual void onFrameBorder();

    /**
     * The window is being terminated.  Do some cleanups here...
     */
    virtual void onClose();

    virtual Margins getFrameMargins() const;
    virtual Frame* getFrame();

    int show(int x, int y, int w, int h, int flag, int exflag, const char* cls, const char* title, Frame* parent);
    void terminate();

private:
    TopWindowImpl* mpImpl;
};

}

#endif
